from blackbox_svl import __version__
from blackbox_svl.SVLSimulation import SVLSimulation


def test_version():
    assert __version__ == '0.1.1'
    
def test_connection_to_simulator():
    sim_connection = SVLSimulation.connect_to_simulator()
    assert sim_connection is not None

def test_trace_collection():
    svl_sim = SVLSimulation("configuration.json", "10.218.101.181")
    trace = None
    trace = svl_sim.run_test(ego_init_speed_m_s = 10, ego_x_pos = 10, sim_duration=15)