import json
import lgsvl
class JsonParser:
    def __init__(self, json_file):
        self.json_file = json_file
        
    def getNPCInitialPosition(self):
        with open(self.json_file) as json_data:
            d = json.load(json_data)
            return d['npc_initial_position']

    def getMapId(self):
        with open(self.json_file) as json_data:
            d = json.load(json_data)
            return d['map']['id']

    def getInitialPositionNPC(self):
        npc_state = lgsvl.AgentState()
        with open(self.json_file) as json_data:
            d = json.load(json_data)
            for agent in d['agents']:
                if(agent['type'] == 2):
                    transform = agent['transform']
                    pos = lgsvl.Vector(transform['position']['x'], transform['position']['y'], transform['position']['z'])
                    angle = lgsvl.Vector(transform['rotation']['x'], transform['rotation']['y'], transform['rotation']['z'])
                    npc_state.transform.position = pos
                    npc_state.transform.rotation = angle
                    npc_type = agent['variant']
        return npc_state, npc_type