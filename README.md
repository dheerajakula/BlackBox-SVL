## Installing the Package
- From Gitlab 
    1. Clone the repository:
    `git clone https://gitlab.com/dheerajakula/BlackBox-SVL.git`
    2. Install the necessary packages
        ```
        cd BlackBox-SVL
        poetry install
        ```

## Installing our fork of SVL simulator
1. Clone the repository make sure you have git lfs installed.
`git clone https://github.com/dheerajakula/simulator`
2. Change the current branch.
`git checkout release-2021.1`
3. Open the simulator with Unity Editor Version 2019.4.18f1 and create a python API only simulation.
## Building the logging bridge (Important to collect trace from simulation)
1. First locate the source code for the bridge at Assets/External/Bridges/LoggingBridge
2. Build the Bridge Plugin for use with the Simulator, navigate to the Simulator -> Build Unity Editor menu item. Select LoggingBridge bridge and build. Output bundle will be in AssetBundles/Bridges folder in root of Simulator Unity Project
3. Simulator will load, at runtime, all custom Bridge plugins from AssetBundles/Bridges folder

**Note**: We use this bridge to store the trace at this location`~user/.config/unity3d/LGElectronics/SVLSimulator/simulation_log.json.gz`. This file is read by our blackbox and returned in the required format.

## Running a falsification example
To create a simulation first create the simulation object by providing the configuration file.
`svl_sim = SVLSimulation("configuration.json", "10.218.101.181")`

Then run a simulation by specifying the parameters for the simulation
`result = svl_sim.run_test(ego_init_speed_m_s, ego_x_pos, steptime=times, inp_signal=signal, sim_duration=15, for_matlab=False)`

1. By following a one dimensional signal from psytaliro
`poetry run python examples/FollowInputSignal.py`
2. By following a two dimensional signal from psytaliro
`poetry run python examples/FollowInputSignal2D.py`
3. By following points from psytaliro
`poetry run python examples/FollowPoints.py`

## Creating the configuration file
Use the visual scenario editor form the simulator to create the configuration file. Place the npc vehicle where you would like the npc to start its motion. Then save the generated configuration json file in the path. Read here https://www.svlsimulator.com/docs/creating-scenarios/visual-scenario-editor/ for more information.

![vse editor](visual_scenario_editor.png)

## Running Apollo Autopilot

Follow these instructions to setup and run apollo https://www.svlsimulator.com/docs/system-under-test/apollo-master-instructions/. Now while creating a simulation object use the ip address of the system on which the autopilot is running as the apollo host parameter. "connection to apollo failed" error is raised if apollo is not properly setup.
 `svl_sim = SVLSimulation(config_file = "configuration.json", apollo_host = "10.218.101.181")`.
 
![vse editor](apollo.png). 